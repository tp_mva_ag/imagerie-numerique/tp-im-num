function w = specification_channels(u,v)
w = zeros(size(u));
for i = 1:3
    uch = u(:,:,i);
    vch = v(:,:,i);
    
    size_u = size(uch);
    
    uch = reshape(uch, 1, numel(uch));
    vch = reshape(vch, 1, numel(vch));
    
    [~, u_sort_idx] = sort(uch);
    [~, v_sort_idx] = sort(vch);
    
    uch(u_sort_idx) = vch(v_sort_idx);
    
    w(:,:,i) = reshape(uch, size_u(1), size_u(2));
end

w = uint8(w);
end