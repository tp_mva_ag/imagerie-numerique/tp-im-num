u = imread('images/flower1.bmp');  % use imread with Matlab
v = imread('images/flower2.bmp');
[nr,nc,nch] = size(u);
figure(1); imshow(u, []);
figure(2); imshow(v, []);

w = affine_transfer(u,v);
w = min(max(w,0),255);
figure(3); imshow(w, []);

w2 = specification_channels(u,v);
w2 = min(max(w2,0),255);
figure(4); imshow(w2, []);

figure(5); subplot(1,2,1); histogram(v); title('Histogramme V')
subplot(1,2,2); histogram(w2); title("Histogramme W = 'U apr�s �galisation'")

%% Opponent space

O = [1/sqrt(3), 1/sqrt(3), 1/sqrt(3) ;...
    1/sqrt(2) -1/sqrt(2) 0;...
    1/sqrt(6) 1/sqrt(6) -2/sqrt(6)];

u_opp = reshape(reshape(double(u), nr * nc, 3) * O', nr, nc, 3);      % Matlab : replace 'matrix'  by 'reshape'
v_opp = reshape(reshape(double(v), nr * nc, 3) * O', nr, nc, 3);

w_opp = affine_transfer(u_opp,v_opp);
% w_opp = specification_channels(u_opp, v_opp);

w_opp = reshape(reshape(double(w_opp), nr * nc, 3) * inv(O)', nr, nc, 3);
w_opp = min(max(w_opp, 0), 255);
w_opp = uint8(w_opp);

figure(6); imshow(w_opp, []); title('Opponent color space')

