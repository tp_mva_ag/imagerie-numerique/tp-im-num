function w = affine_transfer(u,v)
w = zeros(size(u));
for i = 1:3
    col_u = double(u(:,:,i));
    col_v = double(v(:,:,i));
    
    size_u = size(col_u);
    
    col_u = reshape(col_u, 1, numel(col_u));
    col_v = reshape(col_v, 1, numel(col_v));
    
    mean_v = mean(col_v);
    mean_u = mean(col_u);
    std_v = std(col_v);
    std_u = std(col_u);
    
    col_u = (std_v / std_u) * (col_u - mean_u) ...
        + mean_v;
    
    w(:,:,i) = reshape(col_u, size_u(1), size_u(2));
end

w = uint8(w);
end
