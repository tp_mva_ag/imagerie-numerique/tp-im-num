figure(1)
a=zeros(256,256);a(63,63)=1;
imshow(a,[]);

figure(2)
imshow(ifft2(a),[]);

%% 

a = double(imread('room.pgm'))/255;  % On ouvre l'image
b = double(imread('lena.pgm'))/255;  % On ouvre l'image

A = fftshift(fft2(a));
B = fftshift(fft2(b));

Ap=exp(1i*angle(A)).*abs(B); % module de la TFD de b et phase de A
Bp=exp(1i*angle(B)).*abs(A);
figure(3)
ap=real(ifft2(Ap)); imshow(ap,[]); title('module de b et phase de a');
figure(4)
bp=real(ifft2(Bp)); imshow(bp,[]); title('module de a et phase de b');