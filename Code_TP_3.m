imrgb = imread('parrot.bmp');  % load an image
[nrow,ncol,nchan] = size(imrgb);  % image size
figure(1);
subplot(2,2,1); imshow(imrgb,[]), title('color image')
subplot(2,2,2), imshow(imrgb(:,:,1),[]), title('red channel');
subplot(2,2,3), imshow(imrgb(:,:,2),[]), title('green channel');
subplot(2,2,4), imshow(imrgb(:,:,3),[]), title('blue channel');

imgray = sum(imrgb,3)/3;  
imrgb = double(imrgb);  

red = imrgb(:,:,1);
green = imrgb(:,:,2);
blue = imrgb(:,:,3);

%% Opponent spaces

O1 = 1/sqrt(2) * (red-green);
O2 = 1/sqrt(6) * (red+green-2*blue);
O3 = 1/sqrt(3) * (red+green+blue);

figure(2);
subplot(2,2,1); imshow(imrgb,[0,255]), title('color image')
subplot(2,2,2), imshow(O1,[]), title('O1 channel');
subplot(2,2,3), imshow(O2,[]), title('O2 channel');
subplot(2,2,4), imshow(O3,[]), title('O3 channel');

%% HSI space
% 
% H = atan(O1./O2);
% S = sqrt(O1.^2 + O2.^2);
% I = O3;
% 
% figure(3);
% subplot(2,2,1); imshow(imrgb,[0,255]), title('color image')
% subplot(2,2,2), imshow(H,[]), title('H channel');
% subplot(2,2,3), imshow(S,[]), title('S channel');
% subplot(2,2,4), imshow(I,[]), title('I channel');

% Bis

imhsv = rgb2hsv(imrgb);
figure(4);
subplot(2,2,1); imshow(imhsv,[0,255]), title('color image')
subplot(2,2,2), imshow(imhsv(:,:,1),[]), title('H channel');
subplot(2,2,3), imshow(imhsv(:,:,2),[]), title('S channel');
subplot(2,2,4), imshow(imhsv(:,:,3),[]), title('V channel');

%% Space of perceived colors

close all; clear all;
u=[0:1:1000];
r=exp(-(u-440).^2/(2*40^2));
g=exp(-(u-540).^2/(2*40^2));
b=exp(-(u-570).^2/(2*40^2));
figure; plot3(r(380:700),g(380:700),b(380:700),'kd');

%% Image in a 3D space

u = double(imread('simpson512.png'));
[nr,nc,nch] = size(u);
rgb = reshape(u,nr*nc,nch);
figure(1)
plot3(rgb(:,1),rgb(:,2),rgb(:,3),'.');

K = 100;
[IDX,C] = kmeans(rgb,K,'Start','sample');
h=zeros(K,1);
for i=1:K
    t = find(IDX==i);
    h(i) = length(t);
end
figure(2)
scatter3(C(:,1),C(:,2),C(:,3),[],h);

%% White balance

