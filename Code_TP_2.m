imrgb = double(imread('parrot.bmp'));  % load an image

[nrow,ncol,nchan] = size(imrgb);  % image size
figure(1);
subplot(2,2,1); imshow(imrgb, []), title('color image')
subplot(2,2,2), imshow(imrgb(:,:,1),[]), title('red channel');
subplot(2,2,3), imshow(imrgb(:,:,2),[]), title('green channel');
subplot(2,2,4), imshow(imrgb(:,:,3),[]), title('blue channel');

%% 

imgray = sum(imrgb,3)/3;  

imhisto = hist(imgray(:),0:255);  % or imhist(imgray(:),256); with Scilab 
imhisto =imhisto/sum(imhisto);
imhistocum = cumsum(imhisto); 

figure(2); subplot(2,2,1); imshow(imgray,[]), title('image');
subplot(2,2,2), bar(imhisto), axis square, title('histogram'); 
subplot(2,2,3), bar(imhistocum), axis square, title('cumulative histogram');

%% 

imeq = imhistocum(uint8(imgray)+1)*255; 
% if imgray is between 0 and 1, multiply it by 255 before applying uint8
figure(3), imshow(imeq,[0,255]); % or fview(imeq) on Scilab; 
title('Egalisation histogramme')

%%

clear all

% Applied to parrot dark and parrot bright
figure(4)
imrgb_dark = imread('parrot_dark.bmp');  % load an image
imgray_dark = sum(imrgb_dark,3)/3;  

imhisto = hist(imgray_dark(:),0:255);  % or imhist(imgray(:),256); with Scilab 
imhisto =imhisto/sum(imhisto);
imhistocum = cumsum(imhisto); 
imeq_dark = imhistocum(uint8(imgray_dark)+1)*255; 

subplot(2,2,1), imshow(imgray_dark,[0,255]);title('dark')
subplot(2,2,2), imshow(imeq_dark,[0,255]);title('dark eq')

imrgb_bright = imread('parrot_bright.bmp');  % load an image
imgray_bright = sum(imrgb_bright,3)/3;  

imhisto = hist(imgray_bright(:),0:255);  % or imhist(imgray(:),256); with Scilab 
imhisto =imhisto/sum(imhisto);
imhistocum = cumsum(imhisto); 
imeq_bright = imhistocum(uint8(imgray_bright)+1)*255; 

subplot(2,2,3), imshow(imgray_bright,[0,255]);title('bright')
subplot(2,2,4), imshow(imeq_bright,[0,255]);title('bright eq')

%%

u = sum(double(imread('buenosaires3.bmp')),3)/3;            
v = sum(double(imread('buenosaires4.bmp')),3)/3; 
[~,index_u]  = sort(u(:)); [v_sort,~]  = sort(v(:));
uspecifv(index_u) = v_sort;
uspecifv = reshape(uspecifv,size(u)); 

uMean = u - (mean(mean(u)) - mean(mean(v)));

figure(5); subplot(2,2,1), imshow(u,[]), title('image u');
subplot(2,2,2), imshow(v,[]), title('image v');
subplot(2,2,3), imshow(uspecifv,[]), title('image specification');
subplot(2,2,4), imshow(uMean,[0,255]), title('image specification');
linkaxes

%% 

u             = sum(double(imread('buenosaires3.bmp')),3)/3;
v             = sum(double(imread('buenosaires4.bmp')),3)/3; 
[u_sort,index_u]  = sort(u(:));
[v_sort,index_v]  = sort(v(:));
u_midway(index_u) = (u_sort + v_sort)/2;
v_midway(index_v) = (u_sort + v_sort)/2;
u_midway = reshape(u_midway,size(u));
v_midway = reshape(v_midway,size(v));

figure(6); subplot(2,2,1), imshow(u,[]), title('image u');
subplot(2,2,2), imshow(v,[]), title('image v');
subplot(2,2,3), imshow(u_midway,[]), title('image u_m_i_d_w_a_y');
subplot(2,2,4), imshow(v_midway,[]), title('image u_m_i_d_w_a_y');

%%

sigma=[1 5 10 20];

img = double(imread('lena.bmp'));

figure(7); 
subplot(5,2,1); imshow(img,[0,255]);title('Gaussian noise')
subplot(5,2,2); plot(hist(img(:),0:255));

for i=1:4
    imgNoise = img + sigma(i)*randn(size(img));
    subplot(5,2,2*i+1); imshow(imgNoise,[0,255]);
    subplot(5,2,2*i+2); plot(hist(imgNoise(:),0:255));
end


figure(8); 
subplot(5,2,1); imshow(img,[0,255]);title('Impulse noise')
subplot(5,2,2); plot(hist(img(:),0:255));

for i=1:4
    imgNoise = img + sigma(i)*rand(size(img));
    subplot(5,2,2*i+1); imshow(imgNoise,[0,255]);
    subplot(5,2,2*i+2); plot(hist(imgNoise(:),0:255));
end


